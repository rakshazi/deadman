FROM golang:alpine AS builder

RUN apk --no-cache add git; \
    mkdir -p /go/src/github.com/jackc/tern/; \
    cd /go/src/github.com/jackc/tern/; \
    git clone https://github.com/jackc/tern.git .; \
    CGO_ENABLED=0 GOOS=linux go build -ldflags="-w -s" -v -a -o tern;

WORKDIR /go/src/gitlab.com/rakshazi/deadman/
COPY . .
RUN go get github.com/jackc/tern; go install github.com/jackc/tern; \
    CGO_ENABLED=0 GOOS=linux go build -ldflags="-w -s" -tags=jsoniter -v -a -o deadman

FROM alpine:latest

CMD ["/opt/app/deadman"]
WORKDIR /opt/app
EXPOSE 8080

RUN apk --no-cache add ca-certificates tzdata; update-ca-certificates; \
    adduser -D -g '' app
COPY --from=builder /go/src/github.com/jackc/tern/tern /opt/app/tern
COPY --from=builder /go/src/gitlab.com/rakshazi/deadman/deadman /opt/app/deadman
COPY ./tern.conf /opt/app
COPY ./db/migrations /opt/app/migrations
COPY ./static /opt/app/static

USER app
