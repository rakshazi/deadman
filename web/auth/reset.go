package auth

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/rakshazi/deadman/db"
	"gitlab.com/rakshazi/deadman/notify"
)

// ResetRequestValidation validation
type ResetRequestValidation struct {
	Email string `form:"email" binding:"required"`
}

// ResetValidation struct
type ResetValidation struct {
	Password string `form:"password" binding:"required"`
}

// ResetRequestForm - show form
func ResetRequestForm(c *gin.Context) {
	c.HTML(http.StatusOK, "auth/reset-request", nil)
}

// ResetForm - show form
func ResetForm(c *gin.Context) {
	code := c.Param("code")
	c.HTML(http.StatusOK, "auth/reset", gin.H{"code": code})
}

// ResetRequest - create reset request
func ResetRequest(c *gin.Context) {
	var form ResetRequestValidation
	if err := c.ShouldBind(&form); err != nil {
		c.Redirect(http.StatusFound, "/auth/reset")
		return
	}
	user := db.FindUserBy("email", c.PostForm("email"))
	if user == nil {
		c.Redirect(http.StatusFound, "/auth/reset")
		return
	}

	if err := user.SetResetCode(); err != nil {
		c.Redirect(http.StatusFound, "/auth/reset")
		return
	}

	uri := "https://" + c.Request.Host + "/auth/reset/" + *user.ResetCode
	message := "Hey, you requested password reset, here is a link for you: " + uri
	notify.Mail([]string{user.Email}, "Password reset", message)

	c.Redirect(http.StatusFound, "/auth/")
}

// Reset - reset password
func Reset(c *gin.Context) {
	var form ResetValidation
	if err := c.ShouldBind(&form); err != nil {
		c.Redirect(http.StatusFound, "/auth/reset")
		return
	}
	user := db.FindUserBy("reset_code", c.Param("code"))
	if user == nil {
		c.Redirect(http.StatusFound, "/auth/reset")
		return
	}
	if err := user.ResetPassword(c.PostForm("password")); err != nil {
		c.Redirect(http.StatusFound, "/auth/reset")
		return
	}

	SessionManager.RenewToken(c.Request.Context())
	SessionManager.Put(c.Request.Context(), "id", user.ID)
	c.Redirect(http.StatusFound, "/switch/")
}
