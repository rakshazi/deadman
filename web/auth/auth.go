package auth

import (
	"fmt"
	"net/http"

	"github.com/alexedwards/scs/v2"
	"github.com/gin-gonic/gin"
	"gitlab.com/rakshazi/deadman/db"
	"golang.org/x/crypto/bcrypt"
)

// SessionManager ...
var SessionManager *scs.SessionManager

// Validation validation
type Validation struct {
	Email    string `form:"email" binding:"required"`
	Password string `form:"password" binding:"required"`
}

// Form - show form
func Form(c *gin.Context) {
	c.HTML(http.StatusOK, "auth", nil)
}

// LoginOrRegister user
func LoginOrRegister(c *gin.Context) {
	var form Validation
	if err := c.ShouldBind(&form); err != nil {
		c.Redirect(http.StatusFound, "/auth/")
	}
	user := db.FindUserBy("email", c.PostForm("email"))
	if user == nil {
		user = db.CreateUser(c.PostForm("email"), c.PostForm("password"))
	}

	err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(c.PostForm("password")))
	if err != nil {
		c.Redirect(http.StatusFound, "/auth/")
		return
	}
	SessionManager.RenewToken(c.Request.Context())
	SessionManager.Put(c.Request.Context(), "id", user.ID)
	c.Redirect(http.StatusFound, "/switch/")
}

// Logout user
func Logout(c *gin.Context) {
	err := SessionManager.Destroy(c.Request.Context())
	if err != nil {
		fmt.Println("[AUTH]", err)
	}
	c.Redirect(http.StatusFound, "/")
}
