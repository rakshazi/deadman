package index

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// Ping healthcheck
func Ping(c *gin.Context) {
	c.JSON(200, gin.H{"message": "pong"})
}

// Index / home page
func Index(c *gin.Context) {
	c.HTML(http.StatusOK, "index", nil)
}
