// Package webswitch - browser representation of deadswitches
package webswitch

import (
	"net/http"

	"github.com/alexedwards/scs/v2"
	"github.com/gin-gonic/gin"
	"gitlab.com/rakshazi/deadman/db"
)

// SessionManager ...
var SessionManager *scs.SessionManager

// SwitchValidation validation
type SwitchValidation struct {
	// TODO: parse recipients by protocol, eg: user@server.com = email, @user:server.com = matrix, @user@server.com = fediverse
	Recipients []string `form:"recipient" binding:"required"`
	Subject    string   `form:"message" binding:"required"`
	Message    string   `form:"message" binding:"required"`
}

// List switches
func List(c *gin.Context) {
	switches := db.GetSwitches(SessionManager.GetInt(c.Request.Context(), "id"))
	c.HTML(http.StatusOK, "switch/list", gin.H{"loggedIn": true, "switches": switches})
}

// CreateForm a switch
func CreateForm(c *gin.Context) {
	c.HTML(http.StatusOK, "switch/form", gin.H{"loggedIn": true})
}

// Create a switch
func Create(c *gin.Context) {}

// UpdateForm a switch
func UpdateForm(c *gin.Context) {
	c.HTML(http.StatusOK, "switch/form", gin.H{"loggedIn": true})
}

// Update a switch
func Update(c *gin.Context) {}

// Delete switch
func Delete(c *gin.Context) {}
