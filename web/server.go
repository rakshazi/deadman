package web

import (
	"net/http"
	"sync"
	"time"

	"github.com/alexedwards/scs/v2"
	"github.com/gin-contrib/multitemplate"
	"github.com/gin-gonic/gin"
	adapter "github.com/gwatts/gin-adapter"
	"gitlab.com/rakshazi/deadman/api/deadswitch"
	"gitlab.com/rakshazi/deadman/db"
	"gitlab.com/rakshazi/deadman/system"
	"gitlab.com/rakshazi/deadman/web/auth"
	"gitlab.com/rakshazi/deadman/web/index"
	"gitlab.com/rakshazi/deadman/web/webswitch"
)

var sessionManager *scs.SessionManager

// RunServer - starts a webserver
func RunServer(wg *sync.WaitGroup) {
	server := gin.Default()
	hydrate(server)
	server.Run(":8080")
	wg.Done()
}

// hydrate - configure web server
func hydrate(router *gin.Engine) {
	// Sessions
	sessionManager = scs.New()
	sessionManager.Store = system.NewSessionStore(db.Pool)
	sessionManager.Cookie.HttpOnly = true
	sessionManager.Lifetime = 24 * time.Hour
	router.Use(adapter.Wrap(sessionManager.LoadAndSave))

	initRoutes(router)
	initTemplates(router)
}

func initRoutes(router *gin.Engine) {
	router.GET("/", index.Index)
	router.GET("/ping", index.Ping)
	router.Static("/assets", "./static/assets")
	router.StaticFile("/favicon.ico", "./static/assets/logo.png")

	authGroup := router.Group("/auth")
	{
		auth.SessionManager = sessionManager
		authGroup.GET("/", auth.Form)
		authGroup.POST("/", auth.LoginOrRegister)

		authGroup.GET("/reset", auth.ResetRequestForm)
		authGroup.POST("/reset", auth.ResetRequest)

		authGroup.GET("/reset/:code", auth.ResetForm)
		authGroup.POST("/reset/:code", auth.Reset)

		authGroup.GET("/logout", auth.Logout).Use(requireAuth())
	}

	switchGroup := router.Group("/switch").Use(requireAuth())
	{
		webswitch.SessionManager = sessionManager
		switchGroup.GET("/", webswitch.List)

		switchGroup.GET("/create", webswitch.CreateForm)
		switchGroup.POST("/create", webswitch.Create)

		switchGroup.GET("/edit/:id", webswitch.UpdateForm)
		switchGroup.POST("/edit/:id", webswitch.Update)

		switchGroup.GET("/delete/:id", webswitch.Delete)
	}

	apiGroup := router.Group("/api/v1")
	{
		apiGroup.GET("/switch/check/:id", deadswitch.Check)
	}
}

func initTemplates(router *gin.Engine) {
	renderer := multitemplate.NewRenderer()
	renderer.AddFromFiles("index", "static/views/layout.html", "static/views/index.html")
	renderer.AddFromFiles("auth", "static/views/layout.html", "static/views/auth/form.html")
	renderer.AddFromFiles("auth/reset-request", "static/views/layout.html", "static/views/auth/reset-request.html")
	renderer.AddFromFiles("auth/reset", "static/views/layout.html", "static/views/auth/reset.html")
	renderer.AddFromFiles("switch/list", "static/views/layout.html", "static/views/switch/list.html")
	renderer.AddFromFiles("switch/form", "static/views/layout.html", "static/views/switch/form.html")

	router.HTMLRender = renderer
}

// middleware that requires session
func requireAuth() gin.HandlerFunc {
	return func(c *gin.Context) {
		if sessionManager.GetInt(c.Request.Context(), "id") <= 0 {
			c.JSON(http.StatusUnauthorized, gin.H{"error": "Unauthorized"})
			return
		}
		c.Next()
	}
}
