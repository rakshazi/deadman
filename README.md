# Dead man's switch

You will die, aye. You may die in bed on your 100 year birthday or you may die accidentally.
That project is for second scenario. You might wish there was something you had told the people around you.
How you feel, what you regret, what you wish you had said.
Or maybe you just want to make sure your pets are taken care of promptly.
For that, you need a dead man's switch.

## How it works

You write texts to the people you want to tell something in case of samething bad happened to you.
Switch will email you every several days/weeks, asking if you are ok (just click the link in the message).
If you will not confirm that you are alive in 60 days, switch sends messages you wrote to recipients you set.

## Intervals

### Are you ok?
Switch sends "Are you ok?" notification by interval that you set, for example: once a month
If you don't respond to notifcation, it will be resent again after 15 days of first one.
If you don't respond to second notification, it will be resent the last time after 7 days of the second one.
If you don't respond to the third (last) notification, switch will send your messages (after 30 days of first notification)

### pause

You may postpone your switch for any period of time in case you will not be available to respond to them.
Switch will continue work after that pause.

## idea & references

https://www.deadmansswitch.net/

http://www.deadman.io/

# Usage & development

## Configuration

Configuration done via env vars:

* DM_MODE - gin mode, allowed values: `debug`, `test`, `release`
* DM_DB_HOST - PostgreSQL database host, eg: `127.0.0.1`
* DM_DB_PORT - PostgreSQL database port, eg: `5432`
* DM_DB_NAME - PostgreSQL database name, eg: `deadman`
* DM_DB_USER - PostgreSQL database user, eg: `deadman`
* DM_DB_PASSWORD - PostgreSQL database password, generate it with `pwgen -s 64 1`
* DM_SMTP_HOST - email smtp host
* DM_SMTP_PORT - email smtp port
* DM_SMTP_USER - email user
* DM_SMTP_PASSWORD - email password
* DM_SMTP_ENCRYPTION - one of: ssl, tls, none (default: none)

## Run

### Binary, on host OS
Before start, run database migrations, [jackc/tern](https://github.com/jackc/tern) used for that:

```bash
tern migrate -m ./db/migrations
./deadman
```

### Docker

```bash
# run cluster
docker-compose up -d
# run migrations
docker-compose exec -T deadman /opt/app/tern migrate -m ./migrations
```

## TODO
https://github.com/gin-contrib/multitemplate + https://gin-gonic.com/docs/examples/html-rendering/
