package notify

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	mail "github.com/xhit/go-simple-mail"
)

var mailServer *mail.SMTPServer

// initMail server config
func initMail() {
	mailServer = mail.NewSMTPClient()
	mailServer.Host = os.Getenv("DM_SMTP_HOST")
	mailServer.Port, _ = strconv.Atoi(os.Getenv("DM_SMTP_PORT"))
	mailServer.Username = os.Getenv("DM_SMTP_USER")
	mailServer.Password = os.Getenv("DM_SMTP_PASSWORD")
	switch strings.ToLower(os.Getenv("DM_SMTP_ENCRYPTION")) {
	case "ssl":
		mailServer.Encryption = mail.EncryptionSSL
	case "tls":
		mailServer.Encryption = mail.EncryptionTLS
	default:
		mailServer.Encryption = mail.EncryptionNone
	}
	mailServer.Encryption = mail.EncryptionSSL
	mailServer.KeepAlive = false
	mailServer.ConnectTimeout = 60 * time.Second
	mailServer.SendTimeout = 60 * time.Second
}

// Mail - send email
func Mail(to []string, subject, message string) error {
	if mailServer == nil {
		initMail()
	}
	mailClient, err := mailServer.Connect()
	if err != nil {
		fmt.Println("[NOTIFY] mail", err)
		return err
	}

	email := mail.NewMSG()
	email.SetFrom(mailServer.Username)
	email.AddTo(to...)
	email.SetSubject(subject)
	email.SetBody(mail.TextPlain, message)
	if err := email.Send(mailClient); err != nil {
		fmt.Println("[NOTIFY] mail", err)
		return err
	}

	return nil
}
