package cron

import (
	"sync"
	"time"

	"github.com/go-co-op/gocron"
)

// RunScheduler - run cron scheduler
func RunScheduler(wg *sync.WaitGroup) {
	scheduler := gocron.NewScheduler(time.UTC)
	hydrate(scheduler)
	scheduler.StartBlocking()
	wg.Done()
}

// hydrate - add tasks/jobs to scheduler
func hydrate(scheduler *gocron.Scheduler) {

}
