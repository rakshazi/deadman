package system

import (
	"context"
	"fmt"
	"time"

	"github.com/jackc/pgx/v4/pgxpool"
)

// SessionStore - PostgreSQL session store for http sessions
// based on https://github.com/alexedwards/scs/tree/master/postgresstore
// adapted to use with pgxpool
type SessionStore struct {
	pool        *pgxpool.Pool
	stopCleanup chan bool
}

// NewSessionStore returns a new PostgreSQL session store instance,
// with a background cleanup goroutine that runs every 5 minutes
// to remove expired session data
func NewSessionStore(pool *pgxpool.Pool) *SessionStore {
	store := &SessionStore{pool: pool}
	go store.StartCleanup(5 * time.Minute)

	return store
}

// Find returns the data for a given session token from the PostgreSQL session store instance.
// If the session token is not found or is expired, the returned exists flag will
// be set to false.
func (store *SessionStore) Find(token string) (b []byte, exists bool, err error) {
	row := store.pool.QueryRow(context.Background(), "SELECT data FROM sessions WHERE token = $1 AND CURRENT_TIMESTAMP < expires_at", token)
	err = row.Scan(&b)
	if err != nil {
		return nil, false, nil
	}
	return b, true, nil
}

// Commit adds a session token and data to the PostgreSQL session store instance with the
// given expiry time. If the session token already exists, then the data and expiry
// time are updated.
func (store *SessionStore) Commit(token string, b []byte, expiresAt time.Time) error {
	_, err := store.pool.Exec(context.Background(), "INSERT INTO sessions (token, data, expires_at) VALUES ($1, $2, $3) ON CONFLICT (token) DO UPDATE SET data = EXCLUDED.data, expires_at = EXCLUDED.expires_at", token, b, expiresAt)
	if err != nil {
		return err
	}
	return nil
}

// Delete removes a session token and corresponding data from the PostgreSQL session store
func (store *SessionStore) Delete(token string) error {
	_, err := store.pool.Exec(context.Background(), "DELETE FROM sessions WHERE token = $1", token)
	return err
}

// StartCleanup goroutine to remove expired sessions
func (store *SessionStore) StartCleanup(interval time.Duration) {
	store.stopCleanup = make(chan bool)
	ticker := time.NewTicker(interval)
	for {
		select {
		case <-ticker.C:
			err := store.deleteExpired()
			if err != nil {
				fmt.Println("[SESSION]", err)
			}
		case <-store.stopCleanup:
			ticker.Stop()
			return
		}
	}
}

// StopCleanup terminates the background cleanup goroutine for the PostgreSQL session
// store instance. It's rare to terminate this; generally store instances and
// their cleanup goroutines are intended to be long-lived and run for the lifetime
// of your application.
//
// There may be occasions though when your use of the session store is transient.
// An example is creating a new session store instance in a test function. In this
// scenario, the cleanup goroutine (which will run forever) will prevent the
// store object from being garbage collected even after the test function
// has finished. You can prevent this by manually calling StopCleanup.
func (store *SessionStore) StopCleanup() {
	if store.stopCleanup != nil {
		store.stopCleanup <- true
	}
}

func (store *SessionStore) deleteExpired() error {
	_, err := store.pool.Exec(context.Background(), "DELETE FROM sessions WHERE expires_at < CURRENT_TIMESTAMP")
	return err
}
