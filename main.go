package main

import (
	"fmt"
	"os"
	"sync"

	"gitlab.com/rakshazi/deadman/cron"
	"gitlab.com/rakshazi/deadman/db"
	"gitlab.com/rakshazi/deadman/web"
)

func main() {
	var wg sync.WaitGroup
	err := db.Connect()
	if err != nil {
		fmt.Println("[DB]", err)
		os.Exit(1)
	}

	wg.Add(2)
	go web.RunServer(&wg)
	go cron.RunScheduler(&wg)

	wg.Wait()
}
