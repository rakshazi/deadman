package deadswitch

import "github.com/gin-gonic/gin"

// Check - confirm that deadswitch owner is still alive
func Check(c *gin.Context) {}

// List all switches
func List(c *gin.Context) {}
