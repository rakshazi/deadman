package db

import (
	"context"
	"fmt"
	"os"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

// Pool - postgresql connection pool
var Pool *pgxpool.Pool

// Connect to database
func Connect() error {
	var err error
	var config *pgxpool.Config
	dbURL := fmt.Sprintf("user=%s password=%s host=%s port=%s dbname=%s", os.Getenv("DM_DB_USER"), os.Getenv("DM_DB_PASSWORD"), os.Getenv("DM_DB_HOST"), os.Getenv("DM_DB_PORT"), os.Getenv("DM_DB_NAME"))
	config, err = pgxpool.ParseConfig(dbURL)
	if err != nil {
		return err
	}
	config.AfterConnect = func(ctx context.Context, conn *pgx.Conn) error {
		_, err := conn.Exec(ctx, "SET names 'UTF8'")
		return err
	}
	Pool, err = pgxpool.ConnectConfig(context.Background(), config)
	if err != nil {
		return err
	}
	return nil
}
