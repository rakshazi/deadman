package db

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/rakshazi/deadman/system"
	"golang.org/x/crypto/bcrypt"
)

// User model
type User struct {
	ID        int
	Email     string
	Password  string
	ResetCode *string
	CreatedAt time.Time
}

// ResetPassword - set new user password and remove reset_code
func (user *User) ResetPassword(password string) error {
	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		fmt.Println("[DB] User.ResetPassword", err)
		return nil
	}
	user.Password = string(hash)
	tx, err := Pool.Begin(context.Background())
	if err != nil {
		fmt.Println("[DB] User.ResetPassword", err)
		return err
	}
	defer tx.Rollback(context.Background())

	ctag, err := tx.Exec(context.Background(), "UPDATE users SET reset_code = NULL, password = $1 WHERE id = $2", user.Password, user.ID)
	if err != nil {
		fmt.Println("[DB] User.ResetPassword", err)
		return err
	}

	if ctag.RowsAffected() <= 0 {
		fmt.Println("[DB] User.ResetPassword user not saved, affected rows:", ctag.RowsAffected())
		return err
	}

	err = tx.Commit(context.Background())
	if err != nil {
		fmt.Println("[DB] User.ResetPassword", err)
		return err
	}

	return nil
}

// SetResetCode - generate and set password reset code for that user
func (user *User) SetResetCode() error {
	code := system.RandomString(32)
	user.ResetCode = &code

	tx, err := Pool.Begin(context.Background())
	if err != nil {
		fmt.Println("[DB] User.SetResetCode", err)
		return err
	}
	defer tx.Rollback(context.Background())

	ctag, err := tx.Exec(context.Background(), "UPDATE users SET reset_code = $1 WHERE id = $2", user.ResetCode, user.ID)
	if err != nil {
		fmt.Println("[DB] User.SetResetCode", err)
		return err
	}

	if ctag.RowsAffected() <= 0 {
		fmt.Println("[DB] User.SetResetCode user not saved, affected rows:", ctag.RowsAffected())
		return err
	}

	err = tx.Commit(context.Background())
	if err != nil {
		fmt.Println("[DB] User.SetResetCode", err)
		return err
	}

	return nil
}

// CreateUser ...
func CreateUser(email string, password string) *User {
	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		fmt.Println("[DB] CreateUser", err)
		return nil
	}
	tx, err := Pool.Begin(context.Background())
	if err != nil {
		fmt.Println("[DB] CreateUser", err)
		return nil
	}
	defer tx.Rollback(context.Background())

	ctag, err := tx.Exec(context.Background(), "INSERT INTO users (email, password) VALUES($1, $2)", email, string(hash))
	if err != nil {
		fmt.Println("[DB] CreateUser", err)
		return nil
	}

	if ctag.RowsAffected() <= 0 {
		fmt.Println("[DB] CreateUser user not saved, affected rows:", ctag.RowsAffected())
		return nil
	}
	err = tx.Commit(context.Background())
	if err != nil {
		fmt.Println("[DB] CreateUser", err)
		return nil
	}

	return FindUserBy("email", email)
}

// FindUserBy param
func FindUserBy(field, value string) *User {
	user := &User{}
	err := Pool.QueryRow(context.Background(), "SELECT * FROM users WHERE "+field+" = $1", value).Scan(&user.ID, &user.Email, &user.Password, &user.ResetCode, &user.CreatedAt)
	if err != nil {
		fmt.Println("[DB] FindUserBy", err)
		return nil
	}
	return user
}
