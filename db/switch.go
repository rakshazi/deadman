package db

import (
	"context"
	"fmt"
	"time"
)

// DeadSwitch model
type DeadSwitch struct {
	ID               int
	UserID           int
	Recipients       []string
	Subject          string
	EncryptedMessage string
	//DecryptedMessage string
	CreatedAt time.Time
}

// GetSwitches - returns all switches, owned by user
func GetSwitches(userID int) []*DeadSwitch {
	var switches []*DeadSwitch
	rows, err := Pool.Query(context.Background(), "SELECT * FROM switches WHERE user_id = $1", userID)
	if err != nil {
		fmt.Println("[DB] GetSwitches", err)
		return switches
	}
	defer rows.Close()
	for rows.Next() {
		deadswitch := &DeadSwitch{}
		err = rows.Scan(&deadswitch.ID, &deadswitch.UserID, &deadswitch.Recipients, &deadswitch.Subject, &deadswitch.EncryptedMessage, &deadswitch.CreatedAt)
		if err != nil {
			fmt.Println("[DB] GetSwitches", err)
		}
		switches = append(switches, deadswitch)
	}

	if rows.Err() != nil {
		fmt.Println("[DB] GetSwitches", rows.Err())
	}

	return switches
}
