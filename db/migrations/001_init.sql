create table users(
    id SERIAL PRIMARY KEY,
    email VARCHAR UNIQUE NOT NULL,
    password VARCHAR NOT NULL,
    reset_code VARCHAR UNIQUE NULL,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

create table switches(
    id SERIAL PRIMARY KEY,
    user_id INT NOT NULL,
    recipients VARCHAR[] NOT NULL,
    subject VARCHAR NOT NULL,
    message TEXT NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
);

create table checks(
    id SERIAL PRIMARY KEY,
    user_id INT NOT NULL,
    switch_id INT NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    responded_at TIMESTAMP DEFAULT NULL,
    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE,
    FOREIGN KEY (switch_id) REFERENCES switches(id) ON DELETE CASCADE
);

create table activations(
    id SERIAL PRIMARY KEY,
    user_id INT NOT NULL,
    switch_id INT NOT NULL,
    activated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
);

---- create above / drop below ----

drop table users;
drop table switches;
drop table checks;
drop table activations
